SELECT * FROM CUSTOMERS

--CTE
WITH
  Customer_cte (Name, Age, Salary)
  AS
  (
    SELECT Name, Age, Salary
    FROM CUSTOMERS
    WHERE Age IS NOT NULL    
  )
  Select COUNT(Age) As 'Total Customer' FROM Customer_cte

-- Stored Procedure 
CREATE PROCEDURE SELECTALL
AS
SELECT * FROM Customers
GO
EXEC SelectAll;

CREATE PROCEDURE SelectCity @City nvarchar(30)
AS
SELECT * FROM Customers WHERE ADDRESS = @City
GO
EXEC SELECTCity @city = 'Noida'

-- USER DEFIENED FUNCTION
CREATE FUNCTION GetAllCus(@Age INT)  
RETURNS TABLE  
AS  
RETURN  
    SELECT *FROM CUSTOMERS WHERE AGE>=@Age
--TO EXECUTE
SELECT *FROM GetAllCus(25) 

--VIEW
--Create
CREATE VIEW [Mumbai Customers] AS
SELECT Name, Age
FROM Customers
WHERE ADDRESS = 'Mumbai';
SELECT * FROM [Mumbai Customers]
--Alter
CREATE OR ALTER VIEW [Mumbai Customers] AS
SELECT Name, Age, Address, Salary
FROM Customers
WHERE ADDRESS = 'Mumbai';
SELECT * FROM [Mumbai Customers]
--Drop
DROP VIEW [Mumbai Customers]


