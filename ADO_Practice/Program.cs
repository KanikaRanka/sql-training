﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.Common;

namespace ADO_Practice
{
    class Program
    {
        static void Main(string[] args)
        {
            using (SqlConnection con = new SqlConnection("Data Source=KANIKARANKAR091\\SQLEXPRESS2019;Initial Catalog=EMPLOYEE_DETAILS;User Id=sa;Password=Kanika*123;"))
            {
                SqlCommand cmd = new SqlCommand("Select * from CUSTOMERS ", con);
                con.Open();
                // Call ExecuteReader to return a DataReader 
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Console.WriteLine(rdr["Name"].ToString());
                    Console.WriteLine(rdr["ID"]);
                }
                //Release resources
                
                rdr.Close();
               
                
                SqlCommand cmd2 = new SqlCommand("UPDATE Customers SET Address='Bhilwara' WHERE Id=4", con);
                
                cmd2.ExecuteNonQuery();

                Console.WriteLine("Executed");
                con.Close();
              
                
                SqlCommand cmd3 = new SqlCommand("Select Avg(Salary) from Customers", con);
                con.Open();
                //int count1 = Convert.ToInt32(cmd1.ExecuteScalar());
                int avgSalary = Convert.ToInt32(cmd3.ExecuteScalar());
                con.Close();
                Console.WriteLine("Avg Salary:" + avgSalary);

                SqlCommand cmd4 = new SqlCommand("Select Count(*) from Customers ", con);
                con.Open();
                int count = Convert.ToInt32(cmd4.ExecuteScalar());
                con.Close();
                Console.WriteLine("Total Rows:" + count);

            }
            Console.Read();
        }
    }
}
