CREATE DATABASE MY_DATABASE
CREATE TABLE Persons (
    ID int NOT NULL PRIMARY KEY,
    LastName varchar(255) ,
    FirstName varchar(255) NOT NULL,
    Age int CHECK (Age>=18),
	City varchar(255) DEFAULT 'Noida',
	Number int UNIQUE
);

INSERT INTO Persons (ID, FirstName, LastName, Age, City, Number)
VALUES (1, 'Kanika', 'Ranka', 22, '', 1234567890)
INSERT INTO Persons (ID, FirstName, LastName, Age, City, Number)
VALUES ( 2,'Vanika', 'Sahu', 24, 'Udaipur', 23456710)
INSERT INTO Persons (ID, FirstName, LastName, Age, City, Number)
VALUES ( 3,'Anika', 'Jain', 25, 'Bhilwara', 34589021)
INSERT INTO Persons (ID, FirstName, LastName, Age, City, Number)
VALUES ( 4,'Anju', 'Jain', 29, 'Allahabad', 1234567893)
INSERT INTO Persons (ID, FirstName, LastName, Age, City, Number)
VALUES ( 5,'', 'Jain', 29, 'Allahabad', 123456793)
INSERT INTO Persons (ID, FirstName, LastName, Age, City, Number)
VALUES ( 6,'Siya', 'Jain', 29, 'Gurgaon', 13456793)

SELECT * FROM Persons

CREATE TABLE Orders(
	OrderID int NOT NULL PRIMARY KEY,
    OrderNumber int NOT NULL,
    ID int FOREIGN KEY REFERENCES Persons(ID)	
);

INSERT INTO Orders(OrderID, OrderNumber, ID)
VALUES (121, 24, 1)
INSERT INTO Orders(OrderID, OrderNumber, ID)
VALUES (122, 74, 2)
INSERT INTO Orders(OrderID, OrderNumber, ID)
VALUES (123, 64, 3)
INSERT INTO Orders(OrderID, OrderNumber, ID)
VALUES (124, 54, 4)
INSERT INTO Orders(OrderID, OrderNumber, ID)
VALUES (125, 34, 5)


SELECT * FROM Orders

SELECT  Persons.Age, Persons.FirstName
FROM Orders
INNER JOIN Persons
ON Orders.ID = Persons.ID 
GROUP BY Age, FirstName
HAVING Age > 23

SELECT  *
FROM Orders
FULL OUTER JOIN Persons
ON Orders.ID = Persons.ID 

SELECT Persons.FirstName, Persons. LastName, Orders.OrderID
FROM Persons, Orders
WHERE Persons.ID = Orders.ID ORDER BY FirstName

SELECT * FROM Persons
WHERE Age LIKE 22