﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinQ_Demo
{
    public class Student
    {
        public int StudentID { get; set; }
        public string StudentName { get; set; }
        public int age { get; set; }
        public int StandardID { get; set; }

    }
    public class Standard
    {
        public int StandardID { get; set; }
        public string StandardName { get; set; }
    }

    class Program
    {
        static void ReportTypeProperties<T>(T obj)
        {
            Console.WriteLine("Compile-time type: {0}", typeof(T).Name);
            Console.WriteLine("Actual type: {0}", obj.GetType().Name);
        }
        static void Main(string[] args)
        {
            //LinQ query syntax
            string[] names = { "Kanika", "Siya", "Megha", "Ester" };
            var myLinqQuery = from name in names
                              where name.Contains("a")
                              select name;
            foreach (var name in myLinqQuery)
                Console.WriteLine("Names are " + name);

            //LinQ method syntax

            Student[] studentArray = {
                    new Student() { StudentID = 1, StudentName = "John", age = 18, StandardID = 1} ,
                    new Student() { StudentID = 2, StudentName = "Steve",  age = 21, StandardID =2 } ,
                    new Student() { StudentID = 3, StudentName = "Bill",  age = 25, StandardID =3 } ,
                    new Student() { StudentID = 4, StudentName = "Ram" , age = 20 } ,
                    new Student() { StudentID = 5, StudentName = "Ram" , age = 18 } ,
                    new Student() { StudentID = 6, StudentName = "Chris",  age = 17 } ,
                    new Student() { StudentID = 7, StudentName = "Rob",age = 19  } ,
                };
            Standard[] standardArray = {
                new Standard() {StandardID = 1, StandardName = "Standard 1"},
                new Standard() {StandardID = 2, StandardName = "Standard 2"},
                new Standard() {StandardID = 3, StandardName = "Standard 3"}
            };
            
            // LinQ lambda expression
            Student[] teenAgerStudents = studentArray.Where(s => s.age > 12 && s.age < 20).ToArray();
            foreach(var std in teenAgerStudents)
            {
                Console.WriteLine(std.StudentName);
            }

            Student student5 = studentArray.Where(s => s.StudentID == 5).FirstOrDefault();
            Console.WriteLine("5 Student is " + student5.StudentName);

            //Order By
            var orderByDescendingResult = from s in studentArray
                                          orderby s.StudentName descending
                                          select s;
            Console.WriteLine("Desc. Order By Students ");
            foreach (var student in orderByDescendingResult)
            {
                
                Console.WriteLine(student.StudentName + " " + student.age);

            }

            //Thenby
            var thenByResult = studentArray.OrderBy(s => s.StudentName).ThenBy(s => s.age);
            Console.WriteLine("Then By Result: ");
            foreach (var thenResult in thenByResult)
            {                
                Console.WriteLine(thenResult.StudentName +" "+ thenResult.age);
            }
            // Thenby Desc 
            var thenByDescResult = studentArray.OrderBy(s => s.StudentName).ThenByDescending(s => s.age);
            Console.WriteLine("Then By desc Result: ");
            foreach (var thenResult in thenByDescResult)
            { 
                Console.WriteLine(thenResult.StudentName + " " + thenResult.age);
            }
            // Group
            var groupedResult = from s in studentArray
                                group s by s.age;
       
            foreach (var ageGroup in groupedResult)
            {
                Console.WriteLine("Age Group: {0}", ageGroup.Key); 

                foreach (Student s in ageGroup) 
                    Console.WriteLine("Student Name: {0}", s.StudentName);
            }

            //Join
            var innerJoinResult = studentArray.Join(
                      standardArray,  
                      student => student.StandardID,    
                      standard => standard.StandardID,  
                      (student, standard) => new  
                      {
                          studentName = student.StudentName,
                          standardName = standard.StandardName
                      });
            Console.WriteLine("Inner Join Result");
            foreach (var obj in innerJoinResult)
            {
                Console.WriteLine("{0} - {1}", obj.studentName, obj.standardName);
            }
            //Select
            var selectResult = studentArray.Select(s => new {
                Name = s.StudentName,
                Age = s.age
            });
            foreach(var secRes in selectResult)
            {
                Console.WriteLine(secRes.Name + " " + secRes.Age);
            }
            //All
            bool allStudent = studentArray.All(s => s.age > 12 && s.age < 20);
            Console.WriteLine(allStudent);
            //Any
            bool isAnyStudentTeenAger = studentArray.Any(s => s.age > 12 && s.age < 20);
            Console.WriteLine(isAnyStudentTeenAger);
            //Contains
            IList<int> intList = new List<int>() { 1, 2, 3, 4, 5,10 };
            bool result = intList.Contains(10);
            Console.WriteLine(result);
            //Agregate
            string commaSeparatedStudentNames = studentArray.Aggregate<Student, string>(
                                        "Student Names: ",  
                                        (str, s) => str += s.StudentName + ",");
            //Average not supported in Query syntax
            var avgAge = studentArray.Average(s => s.age);

            Console.WriteLine("Average Age of Student: {0}", avgAge);
            //Count
            var totalStudents = studentArray.Count();

            Console.WriteLine("Total Students: {0}", totalStudents);

            var adultStudents = studentArray.Count(s => s.age >= 18);

            Console.WriteLine("Number of Adult Students: {0}", adultStudents);
            //Max
            var oldest = studentArray.Max(s => s.age);

            Console.WriteLine("Oldest Student Age: {0}", oldest);
            //Sum
            var sumOfAge = studentArray.Sum(s => s.age);

            Console.WriteLine("Sum of all student's age: {0}", sumOfAge);
            //ElementAt or Default
            Console.WriteLine("2 Student in intList: {0}", studentArray.ElementAt(2).StudentName);
            //Last or Default
            Console.WriteLine("Last Even Element in intList: {0}",
                                 intList.LastOrDefault(i => i % 2 == 0));
            //Sequence Equal
            IList<string> strList1 = new List<string>() { "One", "Two", "Three", "Four", "Three" };

            IList<string> strList2 = new List<string>() { "Two", "One", "Three", "Four", "Three" };

            bool isEqual = strList1.SequenceEqual(strList2); 
            Console.WriteLine(isEqual);
            //Concat
            var concatString = strList1.Concat(strList2);
            foreach(var str in concatString)
            {
                Console.WriteLine("After Concat " + str);
            }
            //Default Empty
            IList<Student> emptyStudentList = new List<Student>();

            var newStudentList1 = emptyStudentList.DefaultIfEmpty(new Student());

            var newStudentList2 = emptyStudentList.DefaultIfEmpty(new Student()
            {
                StudentID = 0,
                StudentName = ""
            });

            Console.WriteLine("Count: {0} ", newStudentList1.Count());
            Console.WriteLine("Student ID: {0} ", newStudentList1.ElementAt(0));

            Console.WriteLine("Count: {0} ", newStudentList2.Count());
            Console.WriteLine("Student ID: {0} ", newStudentList2.ElementAt(0).StudentID);
            //Distinct
            IList<string> strList = new List<string>() { "One", "Two", "Three", "Two", "Three" };
            IList<string> strList3 = new List<string>() { "Four", "Five", "Six", "Seven", "Eight" };

            IList<int> intList1 = new List<int>() { 1, 2, 3, 2, 4, 4, 3, 5 };

            var distinctList1 = strList.Distinct();

            foreach (var str in distinctList1)
                Console.WriteLine(str);

            var distinctList2 = intList1.Distinct();

            foreach (var i in distinctList2)
                Console.WriteLine(i);
            //skip
            var newList = strList.Skip(2);

            foreach (var str in newList)
                Console.WriteLine(str);
            //skipWhile
            var result1 = strList.SkipWhile((s, i) => s.Length > i);

            foreach (string str in result1)
                Console.WriteLine(str);
            //take
            var newList1 = strList.Take(2);

            foreach (var str in newList1)
                Console.WriteLine(str);

            //let
            var lowercaseStudentNames = from s in studentArray
                                        let lowercaseStudentName = s.StudentName.ToLower()
                                        where lowercaseStudentName.StartsWith("r")
                                        select lowercaseStudentName;

            foreach (var name in lowercaseStudentNames)
                Console.WriteLine(name);
            //into
            var teenAgerStudent = from s in studentArray
                                   where s.age > 12 && s.age < 20
                                   select s
                                    into teenStudents
                                   where teenStudents.StudentName.StartsWith("B")
                                   select teenStudents;


            foreach (Student std in teenAgerStudent)
            {
                Console.WriteLine(std.StudentName);

            }
            //Conversion Operators
            ReportTypeProperties(studentArray);
            ReportTypeProperties(studentArray.AsEnumerable());
            ReportTypeProperties(studentArray.AsQueryable());
            //Cast
            ReportTypeProperties(studentArray);
            ReportTypeProperties(studentArray.Cast<Student>());
            //ToArray ToList ToDicionary
            string[] strArray = strList.ToArray<string>();
            Console.WriteLine("strArray type: {0}", strArray.GetType().Name);

            IList<string> list = strArray.ToList<string>();
            Console.WriteLine("newList type: {0}", newList.GetType().Name);

            IDictionary<int, Student> studentDict =
                                studentArray.ToDictionary<Student, int>(s => s.StudentID);

            foreach (var key in studentDict.Keys)
                Console.WriteLine("Key: {0}, Value: {1}",
                                            key, (studentDict[key] as Student).StudentName);
            //Except
            var resultExp = strList.Except(strList3);

            foreach (string str in resultExp)
                Console.WriteLine(str);

            //Intersect
            var resultIntersect = strList1.Intersect(strList2);

            foreach (string str in resultIntersect)
                Console.WriteLine(str);

            Console.Read();

        }
    }
}
